﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp19
{
    class Point
    {
        public int pointx, pointy;
        public Point(int x,int y) 
        {
            this.pointx = x;
            this.pointy = y;
        }
        public int Pointx {
            get { return this.pointx; }
            set {
                this.pointx = value;
      }
        }
        public int Pointy {
            get { return this.pointy; }
            set {
                this.pointy = value;
                 }
        }
    }
}
