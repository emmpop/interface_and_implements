﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    interface ICompute
    {
        int Id { get; set; } //学号
        string Name { get; set; }//姓名
        double Avg();//平均分
        double Total();//总分
        void print();//输出
    }
}
