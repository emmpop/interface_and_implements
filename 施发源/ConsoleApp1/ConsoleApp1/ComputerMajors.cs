﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    //显示方式实现接口
    class ComputerMajors:ICompute
    {
        int ICompute.Id { get; set; }
        string ICompute.Name { get; set; }
        public double English;//英语成绩
        public double Programming;//编程成绩
        public double Database;//数据库成绩    
        double ICompute.Total()
        {
            return English + Programming + Database;
        }
        double ICompute.Avg()
        {
            return (English + Programming + Database) / 3;
        }
        void ICompute.print()
        {
           
            Console.WriteLine($"英语：{English}\n编程：{Programming}\n数据库：{Database}");
        }
    }
}
