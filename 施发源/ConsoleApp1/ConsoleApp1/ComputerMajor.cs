﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    //隐式方式实现接口
    class ComputerMajor :ICompute
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double English;//英语成绩
        public double Programming;//编程成绩
        public double Database;//数据库成绩    
        public double Avg()
        {
            return Total()/ 3;
        }
        public double Total()
        {
            return English + Programming + Database;
        }
        public void print()
        {
            Console.WriteLine($"学号：{Id}\n姓名：{Name}");
            Console.WriteLine($"英语：{English}\n编程：{Programming}\n数据库：{Database}\n总成绩{Total()}\n平均成绩：{Avg()}");
        }
    }
}
