﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //隐式方式实现接口
            ComputerMajor computerMajor = new ComputerMajor();
            computerMajor.Id = 1;
            computerMajor.Name = "sfy";
            computerMajor.English = 78.5;
            computerMajor.Programming = 90.5;
            computerMajor.Database = 95.5;
            computerMajor.print();
            //显示方式实现接口
            ComputerMajors computerMajors = new ComputerMajors();
            ICompute compute = computerMajors;
            compute.Id = 2;
            compute.Name = "sy";
            computerMajors.English = 95.5;
            computerMajors.Programming = 78.5;
            computerMajors.Database = 90.5;
            Console.WriteLine($"学号：{compute.Id}\n姓名：{compute.Name}");
            compute.print();
            Console.WriteLine($"总分：{compute.Total()}\n平均分：{compute.Avg()}");

        }
    }
}
