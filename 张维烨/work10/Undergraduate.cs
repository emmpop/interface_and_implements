﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work10
{
    class Undergraduate : IMajor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public void Requirement()
        { Console.WriteLine("本科生学制4年，必须修满48学分"); }
    }
}
