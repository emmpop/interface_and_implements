﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work10
{
    class Program
    {
        static void Main(string[] args)
        {
            //隐式实现接口
            Students students = new Students();
            students.Id = 1;
            students.Name = "张三";
            students.Sex = "男";
            students.Age = 19;
            students.CardId = 12345;
            students.English = 120;
            students.Math = 110;
            students.Profession = 230;
            students.Total();


            //显式实现接口
            Teacher teacher = new Teacher();
            IBaseInfo baseInfo = teacher;
            baseInfo.Id = 10001;
            baseInfo.Name = "张三的爸爸";
            baseInfo.Sex = "男";
            baseInfo.Age = 49;
            baseInfo.CardId = 123456;
            teacher.Salary = 3000;
            teacher.OvertimePay = 5000;
            teacher.Bonus = 10000;
            Console.WriteLine("学号：{0} \n姓名：{1}\n性别：{2}\n年龄：{3}\n身份证号码：{4}", baseInfo.Id, baseInfo.Name, baseInfo.Sex, baseInfo.Age, baseInfo.CardId);
            baseInfo.Total();


            //接口下的多态
            IMajor major1 = new Undergraduate();
            major1.Id = 101;
            major1.Name = "牛逼";
            major1.Requirement();

            Console.WriteLine();

            IMajor major2 = new Graduate();
            major2.Id = 102;
            major2.Name = "逼牛";
            major2.Requirement();
            Console.WriteLine();
        }
    }
}
