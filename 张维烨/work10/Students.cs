﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work10
{
    class Students : IBaseInfo //隐式实现接口
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public int Age { get; set; }
        public int CardId { get; set; }
        public double English { get; set; }
        public double Math { get; set; }
        public double Profession { get; set; }
        public void Total()
        {
            double TotalScore = English + Math + Profession;
            Console.WriteLine("学号：{0} \n姓名：{1}\n性别：{2}\n年龄：{3}\n身份证号码：{4}\n总分：{5}\n", Id, Name, Sex, Age, CardId, TotalScore);
        }
    }
}
