﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work10
{
    class Graduate:IMajor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public void Requirement()
        { Console.WriteLine("研究生学制3年，必须修满32学分"); }
    }
}
