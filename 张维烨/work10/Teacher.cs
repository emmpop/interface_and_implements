﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work10
{
    class Teacher : IBaseInfo //显式实现接口
    {
        int IBaseInfo.Id { get; set; }
        string IBaseInfo.Name { get; set; }
        string IBaseInfo.Sex { get; set; }
        int IBaseInfo.Age { get; set; }
        int IBaseInfo.CardId { get; set; }
        public int Salary { get; set; }
        public int OvertimePay { get; set; }
        public int Bonus { get; set; }
        void IBaseInfo.Total()
        {
            double TotalMoney = Salary + OvertimePay + Bonus;
            Console.WriteLine("总工资：{0}\n",TotalMoney);
        }
    }
}
