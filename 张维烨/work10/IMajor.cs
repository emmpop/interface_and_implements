﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work10
{
    interface IMajor
    {
        int Id { get; set; }
        string Name { get; set; }
        void Requirement();
    }
}
