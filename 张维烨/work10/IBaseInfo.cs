﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work10
{
    interface IBaseInfo
    {
        int Id { get; set; }
        string Name { get; set; }
        string Sex { get; set; }
        int Age { get; set; }
        int CardId { get; set; }
        void Total();
    }
}
