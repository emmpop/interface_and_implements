﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Square:IShape
    {
       public double Length { get; set; }  //定义正方形边长

        public double Area
        {
            get

            {

                return Length * Length; //计算正方形面积


            }
        }
    }

    class Rectangle:IShape
    {
        public Rectangle(double length, double width)  //长，宽赋值
        {
            this.Length = length;
            this.Width = width;
        }
        public double Length { get; set; }  //定义长方形的长

        public double Width { get; set; }   //定义长方形的宽



        public double Area
        {

            get
            {

                return Length * Width; //计算长方形的面积
            }
            
        }

    }
}
