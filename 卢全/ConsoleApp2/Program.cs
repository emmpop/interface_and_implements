﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {

            //隐
            /*    Student student = new Student();

                student.BookId = 2;

                student.BookName = "十万个为什么";

                student.StundetId = 1922050446;

                student.StundetName = "小明";

                Console.WriteLine("书编号："+student.BookId);
                Console.WriteLine("书名："+student.BookName);
                Console.WriteLine("学生姓名："+student.StundetName);
                Console.WriteLine("学号："+student.StundetId);
                Console.WriteLine( "归还时间"+DateTime.Now);      */



            //显

            Student student = new Student();

            IBook book = student;  //创立接口实例


            book.BookId = 4;

            book.BookName = "小猪giao奇";

            student.StundetId = 1922054782;

            student.StundetName = "小朱";

            Console.WriteLine("书编号：" + book.BookId);
            Console.WriteLine("书名：" + book.BookName);
            Console.WriteLine("学生姓名：" + student.StundetName);
            Console.WriteLine("学号：" + student.StundetId);
            Console.WriteLine("归还时间" + DateTime.Now);


            //多态

            Square square = new Square();
            square.Length = 4;
            Console.WriteLine("正方形的面积："+square.Area);



            IShape shape1 = new Rectangle(15,10);
           
           Console.WriteLine("长方形的面积："+shape1.Area);
         






        }
    }
}
