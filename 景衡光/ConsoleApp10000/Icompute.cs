﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ConsoleApp10000
{
    enum Color
    {
        pink,
        red,
        yellow,
        white,
        purple,
        black
    }

    enum Sex
    {
        女,
        男,
        人妖=-1
    }
    interface Icompute
    {
        public void Add(double a, double b);
    }

    interface Iuserinf
    {
        public void Name(string name);
        public void Sex(Sex sex);
        public void Color(Color color);
    }


    class Output : Icompute, Iuserinf
    {
        //隐式实现
        double sum;
        public void Add(double math, double english)
        {
            sum = math+english;
            Console.WriteLine("我的总分考了:{0}分",sum);
        }

       

        //显示实现
        void Iuserinf.Name(string name)
        {
            Console.WriteLine("我叫:" + name); 
        }
        void Iuserinf.Sex(Sex sex)
        {
            Console.WriteLine("我是有一个{0}生",sex);
        }
        void Iuserinf.Color(Color color)
        {
            Console.WriteLine("我喜欢的颜色是:{0}",color);
        }
    }
}
