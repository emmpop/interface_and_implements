﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class FuJianren : ICompute
    {
        public string name { get; set; }
        public int Studentnumber { get; set; }
        public string dialect { get; set; }
        public string from { get; set;  }
        public void comefrom()
        {
            Console.WriteLine("来自："+from);
        }
        public void speak()
        {
            Console.WriteLine("说话方式是说："+dialect);
        }
    }
    class Beijing : ICompute 
    {
        public string name { get; set; }
        public int Studentnumber { get; set; }
        public string dialect { get; set; }
        public string from { get; set; }
        public void comefrom()
        {
            Console.WriteLine("来自：" + from);
        }
        public void speak()
        {
            Console.WriteLine("说话方式是说：" + dialect);
        }
    }

}
